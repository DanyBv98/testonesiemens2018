#pragma once
#include "Board.h"
#include "Dealer.h"
#include "Player.h"
#include <iostream>

class SetGame : public GameObject
{
public:
	SetGame(const std::string& name, const std::string& logFilename = "game.log");
	SetGame(const SetGame& other);
	SetGame& operator=(const SetGame& other);
	~SetGame() = default;
	// Returns true if the loop must continue.
	bool gameLoop();
	void findSets(Player& playerWhoFoundTheSet);
	void validateSets(const size_t& cardOneIndex, const size_t& cardTwoIndex, const size_t& cardThreeIndex, Player& playerWhoFoundTheSet);
	void Start();
private:
	bool checkIfSet(const Card& firstCard, const Card& secondCard, const Card& thirdCard);
	Dealer * dealer;
	Board * gameBoard;
};