#pragma once
#include "GameObject.h"
#include <unordered_map>

class Card : public GameObject
{
public:
	enum class Number : unsigned char
	{
		ONE,
		TWO,
		THREE,
		COUNT
	};
	enum class Symbol : unsigned char
	{
		DIAMOND,
		SQUIGGLE,
		OVAL,
		COUNT
	};
	enum class Shading : unsigned char
	{
		SOLID,
		STRIPED,
		OPEN,
		COUNT
	};
	enum class Color : unsigned char
	{
		RED,
		GREEN,
		BLUE,
		COUNT
	};
	Card();
	Card(const Card& other);
	~Card() = default;
	Card& operator=(const Card& other);
	Card(const Number& number, const Symbol& symbol, const Shading& shading, const Color& color);
	Number getNumber() const;
	Symbol getSymbol() const;
	Shading getShading() const;
	Color getColor() const;
private:
	Number number;
	Symbol symbol;
	Shading shading;
	Color color;

	static class EnumConverter {
		public:
			EnumConverter();
			std::string operator()(const Number& number, const Symbol& symbol, const Shading& shading, const Color& color) const;
		private:
			std::unordered_map<Number, std::string> numbers;
			std::unordered_map<Symbol, std::string> symbols;
			std::unordered_map<Shading, std::string> shadings;
			std::unordered_map<Color, std::string> colors;
	} enumConverter;
};