#include "SetGame.h"

SetGame::SetGame(const std::string & name, const std::string& logFilename = "game.log") :
	GameObject(name)
{
}

SetGame::SetGame(const SetGame & other) :
	GameObject(other)
{
	*this = other;
}

SetGame & SetGame::operator=(const SetGame & other)
{
	// The new pointer points to the old address
	// intentionally as the method used is aggregation.
	this->dealer = other.dealer;
	this->gameBoard = other.gameBoard;
	return *this;
}

bool SetGame::gameLoop()
{

	return true;
}

void SetGame::findSets(Player & playerWhoFoundTheSet)
{
	std::vector<Card> cardsOnTheBoard = gameBoard->getCards();
	size_t cardsCount = cardsOnTheBoard.size();
	for (size_t firstCardIndex = 0; firstCardIndex < cardsCount; ++firstCardIndex)
		for (size_t secondCardIndex = 0; secondCardIndex < cardsCount; ++secondCardIndex)
			for (size_t thirdCardIndex = 0; thirdCardIndex < cardsCount; ++thirdCardIndex)
				if (checkIfSet(cardsOnTheBoard[firstCardIndex], cardsOnTheBoard[secondCardIndex], cardsOnTheBoard[thirdCardIndex]))
				{
					// To be continued
				}
}

void SetGame::validateSets(const size_t & cardOneIndex, const size_t & cardTwoIndex, const size_t & cardThreeIndex,  Player & playerWhoFoundTheSet)
{
	std::vector<Card> cards = gameBoard->getCards();
	if (checkIfSet(cards[cardOneIndex], cards[cardTwoIndex], cards[cardThreeIndex]))
	{
		gameBoard->removeCard(cardOneIndex);
		gameBoard->removeCard(cardTwoIndex);
		gameBoard->removeCard(cardThreeIndex);
		playerWhoFoundTheSet.addPoints(1);
	}
}

void SetGame::Start()
{
	while (gameLoop());
}

bool SetGame::checkIfSet(const Card & firstCard, const Card & secondCard, const Card & thirdCard)
{
	if (firstCard.getNumber() == secondCard.getNumber() && secondCard.getNumber() == thirdCard.getNumber())
		return true;
	if (firstCard.getSymbol() == secondCard.getSymbol() && secondCard.getSymbol() == thirdCard.getSymbol())
		return true;
	if (firstCard.getShading() == secondCard.getShading() && secondCard.getShading() == thirdCard.getShading())
		return true;
	if (firstCard.getColor() == secondCard.getColor() && secondCard.getColor() == thirdCard.getColor())
		return true;
	if (firstCard.getNumber() != secondCard.getNumber() && secondCard.getNumber() != thirdCard.getNumber() && firstCard.getNumber() != thirdCard.getNumber())
		return true;
	if (firstCard.getSymbol() != secondCard.getSymbol() && secondCard.getSymbol() != thirdCard.getSymbol() && firstCard.getSymbol() != thirdCard.getSymbol())
		return true;
	if (firstCard.getShading() != secondCard.getShading() && secondCard.getShading() != thirdCard.getShading() && firstCard.getShading() != thirdCard.getShading())
		return true;
	if (firstCard.getColor() != secondCard.getColor() && secondCard.getColor() != thirdCard.getColor() && firstCard.getColor() != thirdCard.getColor())
		return true;
	return false
}
