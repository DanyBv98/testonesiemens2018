#pragma once
#include "GameObject.h"
#include "Card.h"
#include <vector>

class Board : public GameObject
{
public:
	Board(const std::string& name);
	Board(const Board& other);
	Board& operator=(const Board& other);
	~Board() = default;
	std::vector<Card> getCards() const;
	void removeCard(const size_t& index);
private:
	std::vector<Card> cards;
};