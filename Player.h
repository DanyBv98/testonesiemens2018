#pragma once
#include "GameObject.h"

class Player : public GameObject
{
public:
	Player(const std::string& name);
	~Player() = default;
	void addPoints(const int& pointsModifier);
	int getPoints() const;
private:
	int points;
};