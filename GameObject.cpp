#include "GameObject.h"

const std::string GameObject::keywordToEraseFromTypeName = "class ";

GameObject::GameObject(const std::string & name) :
	name(name)
{
}

GameObject::GameObject(const GameObject & other)
{
	*this = other;
}

GameObject & GameObject::operator=(const GameObject & other)
{
	this->name = other.name;
	return *this;
}

std::string GameObject::toString() const
{
	std::string className = std::string(typeid(*this).name());
	auto classKeywordPosition = className.find(keywordToEraseFromTypeName);
	if (classKeywordPosition != std::string::npos)
		className.erase(classKeywordPosition, keywordToEraseFromTypeName.size());
	return className + " (" + name + ")";
}

std::string GameObject::getName() const
{
	return name;
}
