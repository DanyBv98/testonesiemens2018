#include "Deck.h"
#include <iostream>
#include <random>
#include <algorithm>

const size_t Deck::numberOfCards;

Deck::Deck(const std::string& name) :
	GameObject(name)
{
	populateDeck();
	shuffleDeck();
	currentCard = cards.begin();
}

Deck::Deck(const Deck & other) :
	GameObject(other)
{
	*this = other;
}

Deck & Deck::operator=(const Deck & other)
{
	GameObject::operator=(other);
	this->cards = other.cards;
	return *this;
}

void Deck::populateDeck()
{
	size_t cardsAdded = 0;
	size_t numbersCount = static_cast<size_t>(Card::Number::COUNT);
	size_t symbolsCount = static_cast<size_t>(Card::Symbol::COUNT);
	size_t shadingsCount = static_cast<size_t>(Card::Shading::COUNT);
	size_t colorsCount = static_cast<size_t>(Card::Color::COUNT);
	for (size_t numberIndex = 0; numberIndex < numbersCount; ++numberIndex)
		for (size_t symbolIndex = 0; symbolIndex < symbolsCount; ++symbolIndex)
			for (size_t shadingIndex = 0; shadingIndex < shadingsCount; ++shadingIndex)
				for (size_t colorIndex = 0; colorIndex < colorsCount; ++colorIndex)
				{
					cards[cardsAdded++] = Card(static_cast<Card::Number>(numberIndex),
						static_cast<Card::Symbol>(symbolIndex), static_cast<Card::Shading>(shadingIndex),
						static_cast<Card::Color>(colorIndex));
				}
}

void Deck::shuffleDeck()
{
	std::shuffle(cards.begin(), cards.end(), std::mt19937{ std::random_device{}() });
}

