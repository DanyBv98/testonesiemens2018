#include "Player.h"

Player::Player(const std::string & name) :
	GameObject(name)
{
}

void Player::addPoints(const int & pointsModifier)
{
	points += pointsModifier;
}

int Player::getPoints() const
{
	return points;
}
