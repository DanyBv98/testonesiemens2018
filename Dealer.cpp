#include "Dealer.h"

Dealer::Dealer(const std::string& name, Deck* deck) :
	GameObject(name), deck(deck)
{
}

Dealer::Dealer(const Dealer & other) :
	GameObject(other)
{
	*this = other;
}

Dealer & Dealer::operator=(const Dealer & other)
{
	GameObject::operator=(other);
	// The new pointer points to the old address
	// intentionally as the method used is aggregation.
	this->deck = other.deck;
	return *this;
}

std::vector<Card> Dealer::dealCards(const size_t & count)
{
	std::vector<Card> returnedCards;
	for (std::size_t index = 0; deck->currentCard != deck->cards.end() && index < count; ++deck->currentCard, ++index)
		returnedCards.push_back(*deck->currentCard);
	return returnedCards;
}
