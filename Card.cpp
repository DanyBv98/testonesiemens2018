#include "Card.h"

Card::EnumConverter Card::enumConverter;

Card::Card() :
	GameObject("invalid")
{
}

Card::Card(const Card & other) :
	GameObject(other)
{
	*this = other;
}

Card & Card::operator=(const Card & other)
{
	GameObject::operator=(other);
	this->number = other.number;
	this->symbol = other.symbol;
	this->shading = other.shading;
	this->color = other.color;
	return *this;
}

Card::Card(const Number & number, const Symbol & symbol, const Shading & shading, const Color & color) :
	GameObject(enumConverter(number, symbol, shading, color)), 
	number(number), 
	symbol(symbol), 
	shading(shading), 
	color(color)
{
}

Card::Number Card::getNumber() const
{
	return number;
}

Card::Symbol Card::getSymbol() const
{
	return symbol;
}

Card::Shading Card::getShading() const
{
	return shading;
}

Card::Color Card::getColor() const
{
	return color;
}

std::string Card::EnumConverter::operator()(const Number & number, const Symbol & symbol, const Shading & shading, const Color & color) const
{
	return std::string(numbers.at(number) + " " + symbols.at(symbol) + " " + shadings.at(shading) + " " + colors.at(color));
}

Card::EnumConverter::EnumConverter() :
	numbers({
		{ Number::ONE, "One" },
		{ Number::TWO, "Two" },
		{ Number::THREE, "Three" }
	}),
	symbols({
		{ Symbol::DIAMOND, "Diamond" },
		{ Symbol::OVAL, "Oval" },
		{ Symbol::SQUIGGLE, "Squiggle" }
	}),
	shadings({
		{ Shading::OPEN, "Open" },
		{ Shading::SOLID, "Solid" },
		{ Shading::STRIPED, "Striped" }
	}),
	colors({
		{ Color::BLUE , "Blue" },
		{ Color::GREEN , "Green" },
		{ Color::RED , "Red" }
	})
{
}