#pragma once
#include <array>
#include "GameObject.h"
#include "Card.h"

class Dealer;

class Deck : public GameObject
{
public:
	Deck(const std::string& name);
	Deck(const Deck& other);
	Deck& operator=(const Deck& other);
	static const size_t numberOfCards = 81;
private:
	std::array<Card, numberOfCards> cards;
	std::array<Card, numberOfCards>::iterator currentCard;
	void populateDeck();
	void shuffleDeck();
	friend class Dealer;
};