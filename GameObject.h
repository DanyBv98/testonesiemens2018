#pragma once
#include <string>

class GameObject
{
public:
	GameObject(const std::string& name);
	GameObject(const GameObject& other);
	~GameObject() = default;
	GameObject& operator=(const GameObject& other);
	virtual std::string toString() const;
	std::string getName() const;
private:
	static const std::string keywordToEraseFromTypeName;
	std::string name;
};