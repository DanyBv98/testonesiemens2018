#include "Board.h"

Board::Board(const std::string & name) :
	GameObject(name)
{
}

Board::Board(const Board & other) :
	GameObject(other)
{
	*this = other;
}

Board & Board::operator=(const Board & other)
{
	this->cards = other.cards;
	return *this;
}

std::vector<Card> Board::getCards() const
{
	return cards;
}

void Board::removeCard(const size_t & index)
{
	cards.erase(cards.begin() + index);
}
